-- for using gist index type we must install this extension 
create extension btree_gist;


CREATE TABLE test.document_number (
	id serial,
	d_seria varchar(10) NOT NULL,
	d_number int4range NOT NULL,
	insert_date timestamp NOT NULL,
	CONSTRAINT document_number_pkey PRIMARY KEY (id),
	-- this constraint control overlapping seria and d_number range 
	CONSTRAINT document_number_range_unique EXCLUDE USING gist (d_seria WITH =, d_number WITH &&)
);
CREATE INDEX document_number_range_unique_idx ON test.document_number USING gist (d_seria, d_number);


insert into test.document_number (id, d_seria, d_number, insert_date)
values (1, 'AC', '[1, 100]', now());
-- Result = OK

insert into test.document_number (id, d_seria, d_number, insert_date)
values (2, 'AC', '[101, 300]', now());
-- Result = OK

insert into test.document_number (id, d_seria, d_number, insert_date)
values (3, 'AC', '[201, 400]', now());
/* Result = error ERROR: conflicting key value violates exclusion constraint "document_number_range_unique"
   Detail: Key (d_seria, d_number)=(AC, [201,401)) conflicts with existing key (d_seria, d_number)=(AC, [101,301)).  */

